import React from 'react';
import Appbar from '@material-ui/core/AppBar';
import { Button, Toolbar } from '@material-ui/core';
import Logo from '../Assets/Images/logo.png';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';


export default function Topbar(){
    return(
        <div>
        <Appbar color="transparent" className="appbar">
        <Toolbar className="header">
        <div>
        <img src={Logo} alt="image" className="logo"/>
        </div>
        <div>
        <Typography >
        <Link href="" className="linkheader"> Learn More </Link>
        <Link href="" className="linkheader" >Contact Us</Link>
        <Button className="linkheader" style={{backgroundColor:"#03ad40", color:"#ffff", borderRadius:"20px", paddingLeft:"10px", paddingRight:"10px", marginRight:"20px"}}>Login</Button>
        <Link href="" className="linkheader">Sign Up</Link>
        </Typography>
        </div>
        </Toolbar>
        <div className="textheader">
        <Typography className="logintext">User Login</Typography>
        </div>
        </Appbar>
        </div>
    )
}