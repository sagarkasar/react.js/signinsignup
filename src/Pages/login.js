import { Toolbar } from '@material-ui/core';
import React from 'react';
import Topbar from '../Pages/topbar';
import { Container, Paper } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import '../Assets/CSS/Custom.css';
import Checkbox from '@material-ui/core/Checkbox';

export default function Login(){
    return(
        <div className="root1">
        
        <Container className="root">
        <Topbar/>
        <div className="form">
        <form className="paper" >
            <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
            <Typography>Enter Email ID</Typography>
            <Input className="input" placeholder="Enter Email"
            disableUnderline
            fullWidth
            />
            </Grid>
            <Grid item xs={12} sm={12} className="viewPass">
            <Typography>Password</Typography>
            <Input className="input" placeholder="Password"
            fullWidth
            disableUnderline
            />
            </Grid>
            </Grid>
            <div className="login-link">
            <Typography ><Checkbox />Remember me</Typography>
            <Typography ><a href="#">Forgot Password</a></Typography>
            </div>
            <div className="btndiv">
            <Button
            style={{backgroundColor:"#03ad40", color:"#ffff", borderRadius:"20px" }}
            type="submit"
            fullWidth
            >
            Login
            </Button>
            <Button
            type="submit"
            fullWidth
            style={{border:"3px solid #e55749", color:"#e55749", borderRadius:"20px", marginTop:"10px"}}
            >
            Sign in with Google
            </Button>
            </div>
        </form>
        </div>
        <div className="regiuser">
        <Typography className="newuser"><a href="#">New User Register Here</a></Typography>
        </div>
        </Container>
        <div className="regiuser">
        <Typography className="footer">@ 2021 spoonfed.com</Typography>
        </div>
        </div>
    );
}